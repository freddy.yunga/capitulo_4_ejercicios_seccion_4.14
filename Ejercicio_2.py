# Ejercicio 2: Desplaza la última línea del programa anterior hacia arriba,
# de modo que la llamada a la función aparezca antes que las deﬁniciones.
# Ejecuta el programa y observa qué mensaje de error obtienes.


repite_estribillo()
def muestra_estribillo():
    print('Soy un leñador, que alegría.')
    print('Duermo toda la noche y trabajo todo el día.')


def repite_estribillo():
    muestra_estribillo()
    muestra_estribillo()


# COMENTARIO
# EL ERROR ES QUE NO SE EJECUTA LA FUNCION PORQUE AL MOMENTO DE LA INVOCACION repite_estribillo() PORQUE NO ESTA DEFINIDA
# DEBE ESTAR PRIMERO DEFINIDA PARA LUEGO SER INVOCADA
