# Ejercicio 7: Reescribe el programa de caliﬁcaciones del capítulo anterior
# usando una función llamada calcula_calificacion, que reciba una
# puntuación como parámetro y devuelva una caliﬁcación como cadena.

def calcula_calificacion(pun):
    try:
        #pun=float(input(" ingrese puntuacion: "))
        if pun >= 0.0 and pun <= 1:
            if pun >= 0.9:
                print(" sobresaliente")
            else:
                if pun >= 0.8:
                    print("notable")
                else:
                    if pun >= 0.7:
                        print("bien")
                    else:
                        if pun >= 0.6:
                            print("suficiente")
                        else:
                            if pun < 0.6:
                                print(" insuficiente")
        else:
            print(" puntuacion incorrecta")
    except:
        print(" puntuacion incorrecta")

if __name__ == '__main__':
    nota=float(input(" ingrese puntuacion: "))
    calcula_calificacion(nota)
