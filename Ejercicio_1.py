# Ejercicio 1: Ejecuta el programa en tu sistema y observa qué números
# obtienes.

import random

for i in range(10):
    x=random.random() # Este programa produce la siguiente lista de 10 números aleatorios entre 0.0 y hasta (pero no incluyendo) 1.0.
    y=random.randint(5, 10)

    #print(i," -> ",x) #imprime numeros aleatorios  entre 0.0 y 1.0
    print("--------------------------------")
    print(" -> ",y)     # La función randint toma los parámetros inferior y superior, y
                        # devuelve un entero entre inferior y superior (incluyendo ambos extremos).
print("--------------------------------")
t = [1, 2, 3]
a=random.choice(t) # escoje un elemento de la tupla
print(" *** >>> ", a)


