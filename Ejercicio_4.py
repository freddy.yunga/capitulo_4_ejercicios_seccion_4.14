
# Ejercicio 4: ¿Cuál es la utilidad de la palabra clave “def” en Python?
# a) Es una jerga que signiﬁca “este código es realmente estupendo”
# b) Indica el comienzo de una función
# c) Indica que la siguiente sección de código indentado debe ser almacenada para
# usarla más tarde
# d) b y c son correctas ambas
# e) Ninguna de las anteriores

# RESOUESTA CORRECTA ES LA B, PORQUE INDICA EL COMIENZO Y LA DEFINICION DE UNA FUNCION.