# Ejercicio 3: Desplaza la llamada de la función de nuevo hacia el ﬁnal,
# y coloca la deﬁnición de muestra_estribillo después de la deﬁnición de
# repite_estribillo. ¿Qué ocurre cuando haces funcionar ese programa?


def repite_estribillo():
    muestra_estribillo()
    muestra_estribillo()

def muestra_estribillo():
    print('Soy un leñador, que alegría.')
    print('Duermo toda la noche y trabajo todo el día.')
repite_estribillo()

# lo que se puede verificar es que imprime de igual forma los mensajes, ya que no importa la ubicacion de las funciones,
# siempre y cuando la invocacion este despues de la declaracion de dichas funciones.