
# @autor: FREDDY YUNGA
# Ejercicio 6: Reescribe el programa de cálculo del salario, con tarifa-ymedia
# para las horas extras, y crea una función llamada calculo_salario
# que reciba dos parámetros (horas y tarifa).
# Introduzca Horas: 45
# Introduzca Tarifa: 10
# Salario: 475.0

print("Ejercicio 1")
def calculo_salario(horas,tarifa):
    if horas <= 40:
        salario = horas * tarifa
        return salario
    else:
        if horas > 40:
            extras = horas - 40
            costo_horaextra = (extras * 1.5) * tarifa
            salario_total = (40 * tarifa) + costo_horaextra
            return salario_total


if __name__ == '__main__':
    h=int(input(" ingrese horas: "))
    t=float(input(" ingrese tarifa: "))
    print(" el salario es:",calculo_salario(h,t))
